import { environment } from '../../environments/environment';

export class ApiUrl {
    private static readonly BASE_SC_USER_URL = `http://${environment.SC_USER_BASE_URL}:${environment.SC_USER_PORT}`;
    private static readonly BASE_API = '/api';
    private static readonly USERS = '/users';
    private static readonly CONNECTED_USER = '/connected-user'
    private static readonly AUTH = '/auth/'
    private static readonly AUTH_SERVER = `http://${environment.AUTH_BASE_URL}:${environment.AUTH_PORT}`;

    static get GET_CONNECTED_USER(): string {
        return `${this.BASE_SC_USER_URL}${this.BASE_API}${this.USERS}${this.CONNECTED_USER}`;
    }

    static get GET_USERS(): string {
        return `${this.BASE_SC_USER_URL}${this.BASE_API}${this.USERS}`;
    }

    static get GET_AUTH_SERVER(): string {
        return `${this.AUTH_SERVER}${this.AUTH}`;
    }
}
