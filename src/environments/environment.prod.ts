// import { version } from '../../package.json';

export const environment = {
  SC_USER_BASE_URL: 'jeannory.dynamic-dns.net',
  SC_USER_PORT: '8081',
  AUTH_BASE_URL: 'jeannory.ovh',
  AUTH_PORT: '8099',
  // APP_VERSION: version,
  production: true,
};
